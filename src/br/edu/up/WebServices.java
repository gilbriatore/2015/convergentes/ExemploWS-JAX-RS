
package br.edu.up;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class WebServices extends Application{

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> set = new HashSet<>();
		set.add(PrimeiroWS.class);
     	return Collections.unmodifiableSet(set); 
	}
	
}
