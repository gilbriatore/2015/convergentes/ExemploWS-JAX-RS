package br.edu.up;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.ws.rs.ext.RuntimeDelegate;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Servidor {

	public static void main(String[] args) throws Exception {

		WebServices webservices = new WebServices();
		HttpHandler handler = RuntimeDelegate.getInstance()
				.createEndpoint(webservices, HttpHandler.class);
		HttpServer server = HttpServer
				.create(new InetSocketAddress(9090), 0);
		server.createContext("/", handler);
		server.start();
		System.out.println("http://localhost:9090/ws");

	}
}