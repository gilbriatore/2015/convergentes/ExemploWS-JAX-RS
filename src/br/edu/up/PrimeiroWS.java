package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ws")
public class PrimeiroWS {
	
	
	@POST
	@Produces({MediaType.TEXT_HTML, 
		      MediaType.APPLICATION_XML,
		      MediaType.APPLICATION_JSON})
	public String getPOST(@HeaderParam("accept") String accept){
		String retorno;
		
		switch (accept) {
		case MediaType.APPLICATION_XML:
			retorno = "<texto>HTML</texto>";
			break;
		case MediaType.APPLICATION_JSON:
			retorno = "{ \"texto\" : \"Json...\"}";
			break;
		default:
			retorno = "<h1>HTML</h1>";
			break;
		}
		return retorno;
	}
	
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getHTML(){
		return "<h1>Algum html...</h1>";
	}
	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getTexto(){
		return "Algum texto...";
	}
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public String getXML(){
		return "<texto>Algum xml...</texto>";
	}
	
	@GET
	@Produces("application/json")
	public String getJSON(){
		return "{ \"texto\" : \"Algum json...\"}";
	}

}
